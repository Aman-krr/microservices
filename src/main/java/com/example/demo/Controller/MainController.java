package com.example.demo.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@GetMapping("testAtEc2")
	public String testApiEc2() {
		return "Hi i am At Ec2";
	}
}
